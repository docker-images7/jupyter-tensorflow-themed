FROM jupyter/tensorflow-notebook

RUN python -m pip install jupyterthemes
RUN jt -f firacode -fs 10 -tfs 11 -nfs 115 -cellw 80% -nf opensans -tf opensans -t monokai -T
