# Docker jupyter-notebook-tensorflow-themed

Docker container identical to jupyter/tensorflow-notebook, execpt including theming via jupyterthemes package

## Usage

### Command line

Run the dockerfile via 
```shell
$ docker run -p 8888:8888 solomoncotton/jupyter-notebook-tensorflow-themed
```
The theme command in the dockerfile is defined via the [jupyterthemes docs](https://github.com/dunovank/jupyter-themes#command-line-examples).

### Docker compose

An example docker-compose usage would be
```yml
version: "3.8"

services:
  jupyter-tensorflow:
    image: solomoncotton/jupyter-notebook-tensorflow-themed
    container_name: jupyter-tensorflow
    ports:
      - 8888:8888
    volumes:
      - /projects/jupyter:/home/joyvan/work
```

This exposes the notebook on port 8888 and maps the container's `work` volume to the host folder `/projects/jupyter`.
